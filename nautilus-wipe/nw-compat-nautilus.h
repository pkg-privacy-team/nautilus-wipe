/*
 *  nautilus-wipe - a nautilus extension to wipe file(s)
 * 
 *  Copyright (C) 2009-2011 Colomban Wendling <ban@herbesfolles.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/* Contains compatibility things for old Nautilus */

#ifndef NW_COMPAT_NAUTILUS_H
#define NW_COMPAT_NAUTILUS_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <glib.h>

G_BEGIN_DECLS


/* Nautilus stuff */

#if ! (defined (HAVE_NAUTILUS_FILE_INFO_GET_LOCATION) && \
       HAVE_NAUTILUS_FILE_INFO_GET_LOCATION)
# undef HAVE_NAUTILUS_FILE_INFO_GET_LOCATION
# define HAVE_NAUTILUS_FILE_INFO_GET_LOCATION 1

#include <gio/gio.h>
#include "nw-api-impl.h"

static GFile *
nautilus_file_info_get_location (NautilusFileInfo *nfi)
{
  GFile *file;
  gchar *uri;
  
  uri = nautilus_file_info_get_uri (nfi);
  file = g_file_new_for_uri (uri);
  g_free (uri);
  
  return file;
}

/* 
 * Workaround for the buggy behavior of g_file_get_path() on the GFile returned
 * by our nautilus_file_info_get_location().
 * Should be harmless in general, and at least for us.
 * 
 * The buggy behavior made g_file_get_path() return the remote path for remote
 * locations, such as "/foo" for "ftp://name.domain.tld/foo", obviously leading
 * to really bad things such as unexpected data loss (by using a local file when
 * the user thinks we use the remote one).
 */
static gchar *
NAUTILUS_WIPE_g_file_get_path (GFile *file)
{
  gchar *path = NULL;
  
  if (g_file_has_uri_scheme (file, "file")) {
    path = g_file_get_path (file);
  }
  
  return path;
}
/* overwrite the GIO implementation */
#define g_file_get_path NAUTILUS_WIPE_g_file_get_path

#endif /* HAVE_NAUTILUS_FILE_INFO_GET_LOCATION */


G_END_DECLS

#endif /* guard */
