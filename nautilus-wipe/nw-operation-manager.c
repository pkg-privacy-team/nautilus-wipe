/*
 *  nautilus-wipe - a nautilus extension to wipe file(s)
 * 
 *  Copyright (C) 2009-2012 Colomban Wendling <ban@herbesfolles.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "nw-operation-manager.h"

#include <stdarg.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#ifdef GDK_WINDOWING_X11
# include <gdk/gdkx.h>
#endif
#ifdef GDK_WINDOWING_WAYLAND
# include <gdk/gdkwayland.h>
#endif
#include <gsecuredelete.h>

#include "nw-progress-dialog.h"


static void
window_realize_callback (GtkWidget *widget,
                         gpointer   data)
{
  GdkWindow    *window;
  const gchar  *native_parent = data;
  
  if (! native_parent) {
    return;
  }
  
  window = gtk_widget_get_window (widget);
  
#ifdef GDK_WINDOWING_X11
  if (GDK_IS_X11_WINDOW (window)) {
    long int  value;
    char     *endptr;
    
    value = strtol (native_parent, &endptr, 10);
    if (endptr == native_parent || *endptr || value < 0) {
      g_warning ("Invalid XID: %s", native_parent);
    } else {
      GdkWindow  *parent_window;
      
      parent_window = gdk_x11_window_foreign_new_for_display (gdk_window_get_display (window),
                                                              (Window) value);
      if (parent_window) {
        gdk_window_set_transient_for (window, parent_window);
      } else {
        g_warning ("Failed to create foreign window for XID %s",
                   native_parent);
      }
    }
  }
#endif
#ifdef GDK_WINDOWING_WAYLAND
  if (GDK_IS_WAYLAND_WINDOW (window)) {
    if (! gdk_wayland_window_set_transient_for_exported (window,
                                                         (gchar *) /* FIXME: non-const!? */ native_parent)) {
      g_warning ("Invalid exported surface: %s", native_parent);
    }
  }
#endif
}

static void
window_set_transient_for_native_handle (GtkWindow    *window,
                                        const gchar  *native_parent)
{
  if (! native_parent) {
    return;
  }
  
  g_return_if_fail (GTK_IS_WINDOW (window));
  
  if (gtk_widget_get_realized (GTK_WIDGET (window))) {
    window_realize_callback (GTK_WIDGET (window), (gpointer) native_parent);
  }
  g_signal_connect_data (window, "realize",
                         G_CALLBACK (window_realize_callback),
                         g_strdup (native_parent), (GClosureNotify) g_free,
                         G_CONNECT_AFTER);
}

/*
 * display_dialog:
 * @parent: Parent window, or %NULL
 * @native_parent: Native parent window handle, or %NULL
 * @type: The dialog type
 * @modal: Whether the dialog should be modal.
 * @title: GtkMessageDialog's title
 * @primary_text: GtkMessageDialog's primary text
 * @secondary_text: GtkMessageDialog's secondary text, or %NULL
 * @first_button_text: Text of the first button, or %NULL
 * @...: (starting at @first_button_text) %NULL-terminated list of buttons text
 *       and response-id.
 * 
 * Returns: The dialog.
 */
static GtkWidget *
display_dialog (GtkWindow       *parent,
                const gchar     *native_parent,
                GtkMessageType   type,
                gboolean         modal,
                const gchar     *title,
                const gchar     *primary_text,
                const gchar     *secondary_text,
                const gchar     *first_button_text,
                ...)
{
  GtkWidget  *dialog;
  va_list     ap;
  
  dialog = gtk_message_dialog_new (parent,
                                   GTK_DIALOG_DESTROY_WITH_PARENT |
                                   (modal ? GTK_DIALOG_MODAL : 0),
                                   type, GTK_BUTTONS_NONE,
                                   "%s", primary_text);
  window_set_transient_for_native_handle (GTK_WINDOW (dialog), native_parent);
  gtk_window_set_title (GTK_WINDOW (dialog), title);
  gtk_window_set_icon_name (GTK_WINDOW (dialog), "nautilus");
  if (secondary_text) {
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                              "%s", secondary_text);
  }
  va_start (ap, first_button_text);
  while (first_button_text) {
    GtkResponseType button_response = va_arg (ap, GtkResponseType);
    
    gtk_dialog_add_button (GTK_DIALOG (dialog), first_button_text, button_response);
    first_button_text = va_arg (ap, const gchar *);
  }
  va_end (ap);
  /* show the dialog */
  g_signal_connect (dialog, "response", G_CALLBACK (gtk_widget_hide), NULL);
  g_signal_connect_after (dialog, "response", G_CALLBACK (gtk_widget_destroy), NULL);
  gtk_widget_show (dialog);
  
  return dialog;
}



/* Gets the last line (delimited by \n) in @str.
 * If the last character is \n, gets the chunk between the the previous \n and
 * the last one.
 * Free the returned string with g_free(). */
static gchar *
string_last_line (const gchar *str)
{
  gchar        *last_line;
  const gchar  *prev_eol  = str;
  const gchar  *eol       = str;
  gsize         i;
  
  for (i = 0; str[i] != 0; i++) {
    if (str[i] == '\n') {
      prev_eol = eol;
      eol = (&str[i]) + 1;
    }
  }
  if (*eol != 0 || eol == str) {
    last_line = g_strdup (eol);
  } else {
    last_line = g_strndup (prev_eol, (gsize)(eol - 1 - prev_eol));
  }
  
  return last_line;
}



struct NwOperationData
{
  NwOperation        *operation;
  gchar              *native_parent;
  NwProgressDialog   *progress_dialog;
  gchar              *title;
  gchar              *failed_primary_text;
  gchar              *success_primary_text;
  gchar              *success_secondary_text;
  gboolean            canceled;
};

/* Frees a NwOperationData structure */
static void
free_opdata (struct NwOperationData *opdata)
{
  if (opdata->progress_dialog) {
    gtk_widget_destroy (GTK_WIDGET (opdata->progress_dialog));
  }
  if (opdata->operation) {
    g_object_unref (opdata->operation);
  }
  g_free (opdata->native_parent);
  g_free (opdata->title);
  g_free (opdata->failed_primary_text);
  g_free (opdata->success_primary_text);
  g_free (opdata->success_secondary_text);
  g_slice_free1 (sizeof *opdata, opdata);
}

/* Displays an operation's error */
static GtkWidget *
display_operation_error (struct NwOperationData  *opdata,
                         gboolean                 is_warning,
                         const gchar             *error)
{
  GtkWidget      *dialog;
  GtkWidget      *content_area;
  GtkWidget      *expander;
  GtkWidget      *scroll;
  GtkWidget      *view;
  GtkTextBuffer  *buffer;
  gchar          *short_error;
  
  dialog = gtk_message_dialog_new (NULL,
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   is_warning ? GTK_MESSAGE_WARNING
                                              : GTK_MESSAGE_ERROR,
                                   GTK_BUTTONS_NONE,
                                   "%s",
                                   is_warning ? opdata->success_primary_text
                                              : opdata->failed_primary_text);
  window_set_transient_for_native_handle (GTK_WINDOW (dialog),
                                          opdata->native_parent);
  gtk_window_set_title (GTK_WINDOW (dialog), opdata->title);
  gtk_window_set_icon_name (GTK_WINDOW (dialog), "nautilus");
  gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
  /* we hope that the last line in the error message is meaningful */
  short_error = string_last_line (error);
  if (is_warning) {
    const gchar *conditional = _("However, the following warning was issued "
                                 "during the operation:");
    
    if (opdata->success_secondary_text) {
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                                "%s\n\n%s\n%s",
                                                opdata->success_secondary_text,
                                                conditional, short_error);
    } else {
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                                "%s\n%s",
                                                conditional, short_error);
    }
  } else {
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                              "%s", short_error);
  }
  g_free (short_error);
  /* add the details expander */
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  expander = gtk_expander_new_with_mnemonic (_("_Details"));
#if GTK_CHECK_VERSION(3, 12, 0)
  gtk_widget_set_margin_start (expander, 12);
  gtk_widget_set_margin_end (expander, 12);
#endif
  gtk_container_add (GTK_CONTAINER (content_area), expander);
  scroll = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroll),
                                       GTK_SHADOW_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (expander), scroll);
  buffer = gtk_text_buffer_new (NULL);
  gtk_text_buffer_set_text (buffer, error, -1);
  view = gtk_text_view_new_with_buffer (buffer);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (view), FALSE);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view), GTK_WRAP_WORD);
  gtk_container_add (GTK_CONTAINER (scroll), view);
  gtk_widget_show_all (expander);
  /* show the dialog */
  g_signal_connect (dialog, "response", G_CALLBACK (gtk_widget_hide), NULL);
  g_signal_connect_after (dialog, "response", G_CALLBACK (gtk_widget_destroy), NULL);
  gtk_widget_show (dialog);
  
  return dialog;
}

static void
operation_finished_handler (GsdAsyncOperation  *operation,
                            gboolean            success,
                            const gchar        *error,
                            gpointer            data)
{
  struct NwOperationData *opdata = data;
  
  gtk_widget_destroy (GTK_WIDGET (opdata->progress_dialog));
  opdata->progress_dialog = NULL;
  
  if (! opdata->canceled) {
    GtkWidget  *dialog;
    
    if (! success || error) {
      dialog = display_operation_error (opdata, success, error);
    } else {
      dialog = display_dialog (NULL, opdata->native_parent,
                               GTK_MESSAGE_INFO, FALSE, opdata->title,
                               opdata->success_primary_text,
                               opdata->success_secondary_text,
                               GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                               NULL);
    }
    g_signal_connect (dialog, "response", G_CALLBACK (gtk_main_quit), NULL);
  } else {
    /* If user canceled, don't show anything more as the outcome is known, yet
     * the exact state we left behind is inherently unknown. */
    gtk_main_quit ();
  }
  free_opdata (opdata);
}

static void
update_operation_progress (struct NwOperationData  *opdata,
                           gdouble                  fraction)
{
  gchar *step = nw_operation_get_progress_step (opdata->operation);
  
  nw_progress_dialog_set_fraction (opdata->progress_dialog, fraction);
  nw_progress_dialog_set_progress_text (opdata->progress_dialog,
                                        step ? "%s" : NULL, step);
  
  g_free (step);
}

static void
operation_progress_handler (GsdAsyncOperation  *operation,
                            gdouble             fraction,
                            gpointer            data)
{
  update_operation_progress (data, fraction);
}

/* sets @pref according to state of @toggle */
static void
pref_bool_toggle_changed_handler (GtkToggleButton *toggle,
                                  gboolean        *pref)
{
  *pref = gtk_toggle_button_get_active (toggle);
}

/* sets @pref to the value of the selected row, column 0 of combo's model */
static void
pref_enum_combo_changed_handler (GtkComboBox *combo,
                                 gint        *pref)
{
  GtkTreeIter   iter;
  
  if (gtk_combo_box_get_active_iter (combo, &iter)) {
    GtkTreeModel *model = gtk_combo_box_get_model (combo);
    
    gtk_tree_model_get (model, &iter, 0, pref, -1);
  }
}

static void
help_button_clicked_handler (GtkWidget *widget,
                             gpointer   data)
{
  GtkWindow  *parent = data;
  GError     *err = NULL;
  
  if (! gtk_show_uri (gtk_widget_get_screen (widget),
                      "help:nautilus-wipe/nautilus-wipe-config",
                      gtk_get_current_event_time (),
                      &err)) {
    /* display the error */
    display_dialog (parent, NULL, GTK_MESSAGE_ERROR, FALSE,
                    gtk_window_get_title (parent),
                    _("Failed to open help"),
                    err->message,
                    GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                    NULL);
    g_error_free (err);
  }
}

typedef void (*NwOperationConfirmCallback) (gboolean                      confirmed,
                                            gboolean                      fast,
                                            GsdSecureDeleteOperationMode  delete_mode,
                                            gboolean                      zeroise,
                                            gpointer                      user_data);

struct NwOperationConfirmData
{
  NwOperationConfirmCallback    callback;
  gpointer                      callback_data;
  gboolean                      fast;
  GsdSecureDeleteOperationMode  delete_mode;
  gboolean                      zeroise;
};

/* Frees a NwOperationData structure */
static void
free_confirm_data (struct NwOperationConfirmData *data)
{
  g_slice_free1 (sizeof *data, data);
}

static void
operation_confirm_dialog_response_handler (GtkDialog *dialog,
                                           gint       response_id,
                                           gpointer   data)
{
  struct NwOperationConfirmData  *confirm_data = data;

  confirm_data->callback (response_id == GTK_RESPONSE_ACCEPT,
                          confirm_data->fast, confirm_data->delete_mode,
                          confirm_data->zeroise, confirm_data->callback_data);
}

/*
 * operation_confirm_dialog:
 * @native_parent: Parent window native handle, or %NULL for none
 * @title: Dialog's title
 * @primary_text: Dialog's primary text
 * @secondary_text: Dialog's secondary text
 * @confirm_button_text: Text of the button to hit in order to confirm (can be a
 *                       stock item)
 * @confirm_button_icon: A #GtkWidget to use as the confirmation button's icon,
 *                       or %NULL for none or the default (e.g. if
 *                       @confirm_button_text is a stock item that have an icon)
 * @fast: default value for the Gsd.SecureDeleteOperation:fast setting
 * @delete_mode: default value for the Gsd.SecureDeleteOperation:mode setting
 * @zeroise: default value for the Gsd.ZeroableOperation:zeroise setting
 * @callback the callback to call when the user answers the question.
 */
static void
operation_confirm_dialog (const gchar                  *native_parent,
                          const gchar                  *title,
                          const gchar                  *primary_text,
                          const gchar                  *secondary_text,
                          const gchar                  *confirm_button_text,
                          GtkWidget                    *confirm_button_icon,
                          gboolean                      default_fast,
                          GsdSecureDeleteOperationMode  default_delete_mode,
                          gboolean                      default_zeroise,
                          NwOperationConfirmCallback    callback,
                          gpointer                      callback_data)
{
  GtkWidget                      *button;
  GtkWidget                      *dialog;
  GtkWidget                      *action_area;
  struct NwOperationConfirmData  *confirm_data;
  
  confirm_data = g_slice_alloc (sizeof *confirm_data);
  confirm_data->callback = callback;
  confirm_data->fast = default_fast;
  confirm_data->delete_mode = default_delete_mode;
  confirm_data->zeroise = default_zeroise;
  confirm_data->callback_data = callback_data;

  dialog = gtk_message_dialog_new (NULL,
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_QUESTION, GTK_BUTTONS_NONE,
                                   "%s", primary_text);
  window_set_transient_for_native_handle (GTK_WINDOW (dialog), native_parent);
  gtk_window_set_title (GTK_WINDOW (dialog), title);
  gtk_window_set_icon_name (GTK_WINDOW (dialog), "nautilus");
  g_signal_connect (dialog, "response", G_CALLBACK (gtk_widget_hide), NULL);
  g_signal_connect_after (dialog, "response", G_CALLBACK (gtk_widget_destroy), NULL);
  g_signal_connect_data (dialog, "response",
                         G_CALLBACK (operation_confirm_dialog_response_handler),
                         confirm_data, (GClosureNotify) free_confirm_data, 0);
  action_area = gtk_dialog_get_action_area (GTK_DIALOG (dialog));
  if (secondary_text) {
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                              "%s", secondary_text);
  }
  /* help button. don't use response not to close the dialog on click */
  button = gtk_button_new_from_stock (GTK_STOCK_HELP);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (help_button_clicked_handler), dialog);
  gtk_box_pack_start (GTK_BOX (action_area), button, FALSE, TRUE, 0);
  if (GTK_IS_BUTTON_BOX (action_area)) {
    gtk_button_box_set_child_secondary (GTK_BUTTON_BOX (action_area), button, TRUE);
  }
  gtk_widget_show (button);
  /* cancel button */
  gtk_dialog_add_button (GTK_DIALOG (dialog),
                         GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT);
  /* launch button */
  button = gtk_dialog_add_button (GTK_DIALOG (dialog),
                                  confirm_button_text, GTK_RESPONSE_ACCEPT);
  if (confirm_button_icon) {
    gtk_button_set_image (GTK_BUTTON (button), confirm_button_icon);
  }
#if GTK_CHECK_VERSION (3, 12, 0)
  gtk_style_context_add_class (gtk_widget_get_style_context (button),
                               GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);
#endif
  /* settings */ {
    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    GtkWidget *expander;
    GtkWidget *box;
    
    expander = gtk_expander_new_with_mnemonic (_("_Options"));
#if GTK_CHECK_VERSION(3, 12, 0)
    gtk_widget_set_margin_start (expander, 12);
    gtk_widget_set_margin_end (expander, 12);
#endif
    gtk_container_add (GTK_CONTAINER (content_area), expander);
    box = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (expander), box);
    /* delete mode option */ {
      GtkWidget        *hbox;
      GtkWidget        *label;
      GtkWidget        *combo;
      GtkListStore     *store;
      GtkCellRenderer  *renderer;
      
      hbox = gtk_hbox_new (FALSE, 5);
      gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, TRUE, 0);
      label = gtk_label_new_with_mnemonic (_("Number of _passes:"));
      gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
      gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
      /* store columns: setting value     (enum)
       *                number of passes  (int)
       *                descriptive text  (string) */
      store = gtk_list_store_new (3, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING);
      combo = gtk_combo_box_new_with_model (GTK_TREE_MODEL (store));
      gtk_label_set_mnemonic_widget (GTK_LABEL (label), combo);
      /* number of passes column */
      renderer = gtk_cell_renderer_text_new ();
      gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
      gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
                                      "text", 1, NULL);
      /* comment column */
      renderer = gtk_cell_renderer_text_new ();
      gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
      gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
                                      "text", 2, NULL);
      /* Adds an item.
       * @value: the setting to return if selected
       * @n_pass: the number of pass this setting shows
       * @text: description text for this setting */
      #define ADD_ITEM(value, n_pass, text)                                    \
        G_STMT_START {                                                         \
          GtkTreeIter iter;                                                    \
                                                                               \
          gtk_list_store_append (store, &iter);                                \
          gtk_list_store_set (store, &iter, 0, value, 1, #n_pass, 2, text, -1);\
          if (value == confirm_data->delete_mode) {                            \
              gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo), &iter);    \
          }                                                                    \
        } G_STMT_END
      /* add items */
      ADD_ITEM (GSD_SECURE_DELETE_OPERATION_MODE_NORMAL,
                38, _("(Gutmann method for old disks)"));
      ADD_ITEM (GSD_SECURE_DELETE_OPERATION_MODE_INSECURE,
                2, _("(advised for modern hard disks)"));
      ADD_ITEM (GSD_SECURE_DELETE_OPERATION_MODE_VERY_INSECURE,
                1, _("(only protects against software attacks)"));
      
      #undef ADD_ITEM
      /* connect change & pack */
      g_signal_connect (combo, "changed",
                        G_CALLBACK (pref_enum_combo_changed_handler),
                        &confirm_data->delete_mode);
      gtk_box_pack_start (GTK_BOX (hbox), combo, FALSE, TRUE, 0);
    }
    /* fast option */ {
      GtkWidget *check;
      
      check = gtk_check_button_new_with_mnemonic (
        _("_Fast and insecure mode (no /dev/urandom, no synchronize mode)")
      );
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
                                    confirm_data->fast);
      g_signal_connect (check, "toggled",
                        G_CALLBACK (pref_bool_toggle_changed_handler),
                        &confirm_data->fast);
      gtk_box_pack_start (GTK_BOX (box), check, FALSE, TRUE, 0);
    }
    /* "zeroise" option */ {
      GtkWidget *check;
      
      check = gtk_check_button_new_with_mnemonic (
        _("Last pass with _zeros instead of random data")
      );
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
                                    confirm_data->zeroise);
      g_signal_connect (check, "toggled",
                        G_CALLBACK (pref_bool_toggle_changed_handler),
                        &confirm_data->zeroise);
      gtk_box_pack_start (GTK_BOX (box), check, FALSE, TRUE, 0);
    }
    gtk_widget_show_all (expander);
  }
  /* run the dialog */
  gtk_widget_show (dialog);
}

static void
operation_confirm_cancel_callback (GtkDialog *dialog,
                                   gint       response_id,
                                   gpointer   data)
{
  struct NwOperationData *opdata = data;
  
  if (response_id == GTK_RESPONSE_ACCEPT) {
    gsd_async_operation_cancel (GSD_ASYNC_OPERATION (opdata->operation));
    nw_progress_dialog_set_progress_text (opdata->progress_dialog,
                                          _("Canceling..."));
    opdata->canceled = TRUE;
  } else if (! g_object_get_data (G_OBJECT (dialog), "was_paused")) {
    gsd_async_operation_resume (GSD_ASYNC_OPERATION (opdata->operation));
  }
}

static void
progress_dialog_response_handler (GtkDialog *dialog,
                                  gint       response_id,
                                  gpointer   data)
{
  struct NwOperationData *opdata = data;
  
  switch (response_id) {
    case GTK_RESPONSE_CANCEL:
    case GTK_RESPONSE_DELETE_EVENT: {
      gboolean    was_paused = nw_progress_dialog_get_paused (NW_PROGRESS_DIALOG (dialog));
      GtkWidget  *confirm_dialog;
      
      if (! was_paused) {
        /* we pause the operation while the user things on whether to really
         * cancel or not, so the  */
        gsd_async_operation_pause (GSD_ASYNC_OPERATION (opdata->operation));
      }
      confirm_dialog = display_dialog (GTK_WINDOW (dialog), NULL,
                                       GTK_MESSAGE_QUESTION, TRUE,
                                       opdata->title,
                                       _("Are you sure you want to cancel this operation?"),
                                       _("Canceling this operation might leave some item(s) in "
                                         "an intermediate state."),
                                       _("Resume operation"), GTK_RESPONSE_REJECT,
                                       _("Cancel operation"), GTK_RESPONSE_ACCEPT,
                                       NULL);
      g_object_set_data (G_OBJECT (confirm_dialog), "was_paused",
                         was_paused ? &was_paused : NULL);
      g_signal_connect (confirm_dialog, "response",
                        G_CALLBACK (operation_confirm_cancel_callback),
                        opdata);
      break;
    }
    
    case NW_PROGRESS_DIALOG_RESPONSE_PAUSE:
      nw_progress_dialog_set_paused (NW_PROGRESS_DIALOG (dialog),
                                     gsd_async_operation_pause (GSD_ASYNC_OPERATION (opdata->operation)));
      break;
    
    case NW_PROGRESS_DIALOG_RESPONSE_RESUME:
      nw_progress_dialog_set_paused (NW_PROGRESS_DIALOG (dialog),
                                     ! gsd_async_operation_resume (GSD_ASYNC_OPERATION (opdata->operation)));
      break;
    
    default:
      break;
  }
}

static void
operation_confirm_callback (gboolean                      confirmed,
                            gboolean                      fast,
                            GsdSecureDeleteOperationMode  delete_mode,
                            gboolean                      zeroise,
                            gpointer                      user_data)
{
  struct NwOperationData *opdata  = user_data;
  GError                 *err     = NULL;
  
  if (! confirmed) {
    free_opdata (opdata);
    gtk_main_quit ();
    return;
  }
  
  /* update the settings */
  g_object_set (opdata->operation,
                "fast", fast,
                "mode", delete_mode,
                "zeroise", zeroise,
                NULL);
  
  if (! gsd_secure_delete_operation_run (GSD_SECURE_DELETE_OPERATION (opdata->operation),
                                         &err)) {
    GtkWidget *dialog;

    if (err->code == G_SPAWN_ERROR_NOENT) {
      gchar *message;
      
      /* Merge the error message with our. Pretty much a hack, but should be
       * correct and more precise. */
      message = g_strdup_printf (_("%s. "
                                   "Please make sure you have the secure-delete "
                                   "package properly installed on your system."),
                                 err->message);
      dialog = display_operation_error (opdata, FALSE, message);
      g_free (message);
    } else {
      dialog = display_operation_error (opdata, FALSE, err->message);
    }
    g_signal_connect (dialog, "response", G_CALLBACK (gtk_main_quit), NULL);
    g_error_free (err);
    free_opdata (opdata);
  } else {
    /* update the initial progress so the step is correct, too */
    update_operation_progress (opdata, 0.0);
    
    gtk_widget_show (GTK_WIDGET (opdata->progress_dialog));
  }
}

/* 
 * nw_operation_manager_run:
 * @parent: Parent window for dialogs
 * @files: List of paths to pass to @operation_launcher_func
 * @confirm_primary_text: Primary text for the confirmation dialog
 * @confirm_secondary_text: Secondary text for the confirmation dialog
 * @confirm_button_text: Text for the confirm button of the confirmation dialog.
 *                       It may be a GTK stock item.
 * @confirm_button_icon: A #GtkWidget to use as the confirmation button's icon,
 *                       or %NULL for none or the default (e.g. if
 *                       @confirm_button_text is a stock item that have an icon)
 * @progress_dialog_text: Text for the progress dialog
 * @operation: (transfer:full): the operation object
 * @failed_primary_text: Primary text of the dialog displayed if operation failed.
 *                       (secondary is the error message)
 * @success_primary_text: Primary text for the the success dialog
 * @success_secondary_text: Secondary text for the the success dialog
 * 
 * 
 */
void
nw_operation_manager_run (const gchar  *native_parent,
                          GList        *files,
                          const gchar  *title,
                          const gchar  *confirm_primary_text,
                          const gchar  *confirm_secondary_text,
                          const gchar  *confirm_button_text,
                          GtkWidget    *confirm_button_icon,
                          const gchar  *progress_dialog_text,
                          NwOperation  *operation,
                          const gchar  *failed_primary_text,
                          const gchar  *success_primary_text,
                          const gchar  *success_secondary_text)
{
  struct NwOperationData *opdata;
  
  opdata = g_slice_alloc (sizeof *opdata);
  opdata->canceled = FALSE;
  opdata->native_parent = g_strdup (native_parent);
  opdata->progress_dialog = NW_PROGRESS_DIALOG (nw_progress_dialog_new (NULL, 0,
                                                                        "%s", progress_dialog_text));
  if (opdata->native_parent) {
    window_set_transient_for_native_handle (GTK_WINDOW (opdata->progress_dialog),
                                            opdata->native_parent);
  }
  gtk_window_set_title (GTK_WINDOW (opdata->progress_dialog), title);
  gtk_window_set_icon_name (GTK_WINDOW (opdata->progress_dialog), "nautilus");
  nw_progress_dialog_set_has_pause_button (opdata->progress_dialog, TRUE);
  nw_progress_dialog_set_has_cancel_button (opdata->progress_dialog, TRUE);
  g_signal_connect (opdata->progress_dialog, "response",
                    G_CALLBACK (progress_dialog_response_handler), opdata);
  opdata->title = g_strdup (title);
  opdata->failed_primary_text = g_strdup (failed_primary_text);
  opdata->success_primary_text = g_strdup (success_primary_text);
  opdata->success_secondary_text = g_strdup (success_secondary_text);
  opdata->operation = operation;
  g_signal_connect (opdata->operation, "finished",
                    G_CALLBACK (operation_finished_handler), opdata);
  g_signal_connect (opdata->operation, "progress",
                    G_CALLBACK (operation_progress_handler), opdata);
  
  nw_operation_add_files (opdata->operation, files);

  operation_confirm_dialog (native_parent, title,
                            confirm_primary_text, confirm_secondary_text,
                            confirm_button_text, confirm_button_icon,
                            FALSE, GSD_SECURE_DELETE_OPERATION_MODE_INSECURE,
                            FALSE, operation_confirm_callback, opdata);
  
  gtk_main ();
}

