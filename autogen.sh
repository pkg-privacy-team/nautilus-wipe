#!/bin/bash

mkdir -p build/aux                    || exit 1
mkdir -p build/m4                     || exit 1
#gtkdocize --flavour no-tmpl || exit 1
autoreconf -vfi                       || exit 1
grep -q YELP_HELP_INIT aclocal.m4 || {
  echo "M4 macro YELP_HELP_INIT missing." >&2
  echo "Please install Yelp development files" >&2
  exit 1
}

#~ ./configure "$@"
